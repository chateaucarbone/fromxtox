# Setup From X computers

Xubuntu 18.04

! Video library of Processing.org must to be updated or found in another place.


## Computer 1 (WebCam > Video projector)

sudo apt-get install mplayer v4l-utils v4l-conf

Use "v4linux control panel application" 

nano ~/.config/autostart/fromX_projection.desktop 

[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=FromX Start Webcam
Comment=Start webcam and settings
Exec=bash /home/fromx/webcam.sh
OnlyShowIn=XFCE;
StartupNotify=false
Terminal=false
Hidden=false

### Webcam settings

outfmt=yuy2
outfmt=h264
outfmt=mjpg

v4l2-ctl --list-devices
HD Pro Webcam C920 (usb-0000:00:14.0-2):


v4l2-ctl --list-ctrls
                     brightness 0x00980900 (int)    : min=0 max=255 step=1 default=128 value=128
                       contrast 0x00980901 (int)    : min=0 max=255 step=1 default=128 value=128
                     saturation 0x00980902 (int)    : min=0 max=255 step=1 default=128 value=128
 white_balance_temperature_auto 0x0098090c (bool)   : default=1 value=0
                           gain 0x00980913 (int)    : min=0 max=255 step=1 default=0 value=255
           power_line_frequency 0x00980918 (menu)   : min=0 max=2 default=2 value=1
      white_balance_temperature 0x0098091a (int)    : min=2000 max=6500 step=1 default=4000 value=3222
                      sharpness 0x0098091b (int)    : min=0 max=255 step=1 default=128 value=255
         backlight_compensation 0x0098091c (int)    : min=0 max=1 step=1 default=0 value=0
                  exposure_auto 0x009a0901 (menu)   : min=0 max=3 default=3 value=3
              exposure_absolute 0x009a0902 (int)    : min=3 max=2047 step=1 default=250 value=600 flags=inactive
         exposure_auto_priority 0x009a0903 (bool)   : default=0 value=1
                   pan_absolute 0x009a0908 (int)    : min=-36000 max=36000 step=3600 default=0 value=0
                  tilt_absolute 0x009a0909 (int)    : min=-36000 max=36000 step=3600 default=0 value=0
                 focus_absolute 0x009a090a (int)    : min=0 max=250 step=5 default=0 value=30 flags=inactive
                     focus_auto 0x009a090c (bool)   : default=1 value=1
                  zoom_absolute 0x009a090d (int)    : min=100 max=500 step=1 default=100 value=100 (entre 100 et 200)
                      led1_mode 0x0a046d05 (menu)   : min=0 max=3 default=0 value=3
                 led1_frequency 0x0a046d06 (int)    : min=0 max=255 step=1 default=0 value=0
                 

### Other commands
v4l2-ctl --device=/dev/video0 --set-fmt-video=width=800,height=600,pixelformat=1

guvcview --device=/dev/video0 --resolution=1920x1080 --audio=none --gui=none --render_window=full

xawtv -f
xawtv -nodga

cvlc v4l2:///dev/video0 -f
cvlc v4l2:///dev/video0 -H
cvlc v4l2:///dev/video0 --demux h264
cvlc v4l2:///dev/video0:chroma=h264:width=800:height=600

gst-launch-1.0 autovideosrc device=/dev/video0 ! autovideosink

mpv /dev/video0 --no-audio --fs

## Computer 2 (Movies > Screen)

Export Processing code with Java.

nano ~/.config/autostart/fromX_screen.desktop 

[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=fromX
Comment=
Exec=/home/fromx/sketchbook/fromx_screen/application.linux64/fromx_screen
OnlyShowIn=XFCE;
StartupNotify=false
Terminal=false
Hidden=false



