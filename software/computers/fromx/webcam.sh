#!/bin/bash

# Fix auto focus and exposure
v4l2-ctl --set-ctrl=focus_auto=0
v4l2-ctl --set-ctrl=exposure_auto=1
v4l2-ctl --set-ctrl=gain=00
v4l2-ctl --set-ctrl=exposure_absolute=10

# Launch Webcam fullscreen
mplayer tv:// -tv driver=v4l2:width=1280:height=720:device=/dev/video0:fps=15 -fs
