# Setup

 
## Setup computers
Play a movie or a sequence of movies in loop at startup.

Installer http://etcher.io pour mettre les Linux sur les SD Card.

BPI : ! Ports USB sensibles !

### Folders
- ~/fromxtox/start.sh : launch script
- ~/fromxtox/test.mov or test.mp4, ... : media files

### Udoo Quad
udooer/udooer

- udoobuntu-udoo-qdl-desktop_2.2.0.img
- etcher > flash microsdcard (10min)
- sudo apt-get update (10min)
- sudo apt-get upgrade (10min)
- keyboard (bottom bar) > french
- desktop black color / bar at top/dynamic length
- sudo nano ~/.config/lxsession/Lubuntu/autostart
'''
	bash /home/udooer/fromxtox/start.sh
'''
- sudo nano /home/udooer/fromxtox/start.sh
'''
	#! /bin/bash

	sleep 7

	while(true)
	do
		gst-launch-1.0 playbin uri=file:///home/udooer/fromxtox/test.mov video-sink=imxipuvideosink audio-sink=alsasink
	done
'''
- (sudo) chmod a+x start.sh


### Raspberry Pi B+ v1.2 (2014) / Raspberry Pi 3
pi/pi

- 2018-11-13-raspbian-stretch.img ou la version desktop
- etcher > flash micro sdcard
- Configurer le pays, langue, clavier, Internet/ update/upgrade > reboot
- Desktop black. activer SSH
- mkdir ~/.config/autostart
- mkdir ~/fromxtox
- sudo nano ~/.config/autostart/start.desktop
'''
	[Desktop Entry]
	Type=Application
	Exec=bash /home/pi/fromxtox/start.sh
'''
- sudo nano /home/pi/fromxtox/start.sh
'''
	#!/bin/bash
	sleep 7
	omxplayer --loop -b /home/pi/fromxtox/test.mp4 &
'''
- chmod u+x start.sh
- chmod u+x /home/pi/fromxtox/* (pour inclure aussi les vidéos)


### Banana Pi M3 v1.2
pi/pi (bananapi par défaut)

! Ports USB sensibles !

- 2018-07-11-ubuntu-16.04-mate-desktop-preview-bpi-m3-sd-emmc.img (moins de problèmes). très long pour valider
- 2016-05-16-debian-8-jessie-mate-gpu-bpi-m3-sd-emmc.img.zip
- etcher > flash micro sdcard
- ? problème allumage avec le clavier
- bureau noir 
- Menu System > Preferences > Hardware > Keyboard > Layout (add French/move up): clavier français
- sudo passwd pi
- mkdir ~/fromxtox

- méthode 1 (ça n'a pas marché la première fois) : nano ~/.config/autostart/start.desktop
'''
	[Desktop Entry]
	Type=Application
	Exec=bash /home/pi/fromxtox/start.sh
	Hidden=false
	X-MATE-Autostart-enabled=true
	Name[en_US]=Start from x to x
	Name=Start fromx to x
	Comment[en_US]=start
	Comment=start
'''

- méthode 2 Menu System > Preferences > Personal > Startup applications
- Ajouter une application, commande : bash /home/pi/fromxtox/start.sh

- sudo nano /home/pi/fromxtox/start.sh
'''
	#!/bin/bash
	sleep 7
	mpv --fullscreen --loop=inf /home/pi/fromxtox/test.mp4 &
'''




**debian8**: Différent: keyboard layout / Accès Wi-Fi galère / mpv. update/upgrade/dist-upgrade/install mpv



### Banana Pi M1 (Armbian)
- Armbian_5.65_Bananapi_Ubuntu_bionic_next_4.14.78

root/1234 (default)
root/rcaarmbpi

Bananian Linux (root/pi)

user: pi/pi
sudo loadkeyes

! a = q
! m = ,


