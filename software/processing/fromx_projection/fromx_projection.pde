import processing.video.*;

Capture cam;

void setup() {
  fullScreen();
  noCursor();
 cam = new Capture(this, 1920,1080,"HD Pro Webcam C920",15);
 // cam = new Capture(this, 1280,720,"HD Pro Webcam C920",15); // 1600x900
  cam.start();
  //background(0);
}

void draw() {
  
  if (cam.available() == true) {
    cam.read();
  }
  //image(cam, 0, 0, width, height);
  set(0, 0, cam);
}
