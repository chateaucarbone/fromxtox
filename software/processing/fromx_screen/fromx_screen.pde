/*
* From X / Screen : Play movies
* http://chateaucarbone.org
*
* Adapted from : http://codelab.fr/5350
*/

import processing.video.*;

int videoNb = 3; // number of movie files (.mp4 here)
int videoIndex = 0; // start index
Movie [] videos = new Movie[videoNb]; // movies container

void setup() {
  size(640,480);
  //fullScreen();
  //noCursor();
  
  // Init movies
  for (int i = 0; i < videoNb; i++) {
    videos[i]=new Movie(this, i+".mp4"); // mp4 movies start from 0
    videos[i].play();
    videos[i].pause();
    //videos[i].volume(0); // no sound
    println("video n°" + i + ": loaded ("+videos[i].duration()+" s)");
  }
  
  // Play first one
  videos[videoIndex].play();
}

void draw() { 
   // change movie just before the end of the last movie
   if ( videos[videoIndex].time() >= videos[videoIndex].duration() - 0.2) {
     videos[videoIndex].pause(); // pause last video
     videoIndex = (videoIndex + 1) % videoNb; // update index
     
     println("playing n°"+ videoIndex);
     
     videos[videoIndex].jump(0);
     videos[videoIndex].play();
     image(videos[videoIndex], 0, 0, width, height);
   } else {
     image(videos[videoIndex], 0, 0, width, height);
   }
}

void movieEvent(Movie m) {
  m.read();
}
