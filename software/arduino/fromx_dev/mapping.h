#ifndef _mapping_h_
#define _mapping_h_

// Ramps Shield setup

// Steppers pins (X, Y, Z)
#define X_STEP  54
#define X_DIR   55
#define X_EN    38

#define Y_STEP  60
#define Y_DIR   61
#define Y_EN    56

#define Z_STEP  46
#define Z_DIR   48
#define Z_EN    62

// Endstop buttons
#define X_MIN   3
#define Y_MIN   14

// Joystick pins (AUX_2)
#define JOY_X       A5
#define JOY_Y       A10
#define JOY_Z       A12
#define JOY_BUTTON  44

#endif
