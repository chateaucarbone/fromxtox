/*
 * FromX Code / CNC 2D
 * http://chateaucarbone.org
 * 
 * Author: Jérôme Abel
 * Adapted from https://photoscs.wordpress.com/2014/03/21/arduino-stepper-code-joystick/
 * 
 * CNC hardware : Mega + Ramps Shield + TB6600 + NEMA17 + pushbuttons
 * Joystick 4D
 */

#include "stepLib.h";
#include "smoothJoystick.h";
#include "mapping.h"; // Wiring

// Speed setup
#define maxSpeed 4000 // speed measured in Hz
#define minSpeed 1 // speed measured in Hz
#define deadband 60 // deadband for consideration of 0 speed

// Bound limit
const long limit_x = 136000; // 1,5m slide
const long limit_y = 67000; // ~80cm slide

// Joystick
int middle_joy = 512;
signed int xSpeed, ySpeed;
unsigned int xVal, yVal;

// Objects
stepMotor mx(X_STEP, X_DIR);
stepMotor my(Y_STEP, Y_DIR);
stepMotor mz(Z_STEP, Z_DIR); // Stepper Z move as Stepper X

joystick jx(JOY_X, middle_joy);
joystick jy(JOY_Y, middle_joy);

// ---------------------------
void setup() {
  // End stop
  pinMode(X_MIN, INPUT_PULLUP);
  pinMode(Y_MIN, INPUT_PULLUP);

  setHome();
}

// ---------------------------
void loop() {

  // Joystick X > Move X & Z
  xVal=jx.smoothRead();
  xSpeed = convertAnalogToSpeed(xVal);
  
  if (mx.getCounter() >= 0 && mx.getCounter() <= limit_x) {
    mx.step(xSpeed);
    mz.step(xSpeed);
  } else if (mx.getCounter() < 0 && xSpeed < 0) {
    mx.setCounter(0);
  } else if (mx.getCounter() > limit_x && xSpeed > 0) {
    mx.setCounter(limit_x);
  }
  
  // Joystick Y > Move Y
  yVal=jy.smoothRead();
  ySpeed = convertAnalogToSpeed(yVal);
  
  if (my.getCounter() >= 0 && my.getCounter() <= limit_y) {
    my.step(ySpeed);
  } else if (my.getCounter() < 0 && ySpeed < 0) {
    my.setCounter(0);
  } else if (my.getCounter() > limit_y && ySpeed > 0) {
    my.setCounter(limit_y);
  }
}

// Set Home : origins (0,0)
void setHome() {

  // Home X
  while (digitalRead(X_MIN)) {
    mx.step(1000);
    mz.step(1000);
  }
  mx.setCounter(0);
  mz.setCounter(0);

  // Home Y
  while (digitalRead(Y_MIN)) {
    my.step(1000);
  }
  my.setCounter(0);
}

// Analog value to a stepper speed with direction
// bounded by a min and max stepper speed
signed int convertAnalogToSpeed(unsigned int analogVal) {
  if (analogVal < middle_joy + deadband && analogVal > middle_joy - deadband) { // we are within deadband, return a 0 speed
    return 0;
  } else if (analogVal >= middle_joy + deadband) { // move in forward direction - return a positive speed
    return map(analogVal, middle_joy + deadband, middle_joy * 2, minSpeed, maxSpeed); // interpolate our joystick value to the proper speed
  } else if (analogVal <= middle_joy - deadband) { // move in reverse direction - return a negative speed
    return -map(analogVal, middle_joy - deadband, 0, minSpeed, maxSpeed); // interpolate our joystick value to the proper speed
  }
}
