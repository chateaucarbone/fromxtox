/* 
 * ToX Code
 * http://chateaucarbone.org
 * 
 * Dev : get speed number from online log file (change wifi setup)
 * 
 * Wemos D1 ESP8266 / MD25 driver Serial / Button interruption
 * From http://www.robot-electronics.co.uk/htm/arduino_examples.htm
 * 
 * Wiring:
 * Arduino      MD25      Power Supply 12V
 * GND          GND
 * RX           TX
 * TX           RX
 *              +         +
 *              -         -
 *              
 * Set MD25 Serial Mode with both jumper = 38400 bps           
 * Mode 1 : 0 (full reverse) - 128 (stop) - 255 (full forward).
 * forward = vers le moteur
 * 
 */

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

// WiFi setup
const char* ssid = "*****"; // Network name
const char* password = "*****"; // Network password
const char* log_file = "http://*****.txt"; // Url of file

// Md25 driver setup
#define CMD                 (byte)0x00              // MD25 command byte of 0
#define WRITESP2            0x32                    // Byte writes speed to motor n°2
#define RESETREG            0x35                    // Byte resets encoders
#define READENC2            0x24                    // Byte reads encoders n°2

// Endstop
int buttonPin = 12;

// Encoder values : minimum/maximum
int minEnc = 200;
int maxEnc = 10000;
int speedM = 20;
boolean isHome = false;
long encValue = 0;


void setup(){
  
  // Internet connection : wait until connected
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
  }
  
  // Button
  pinMode(buttonPin, INPUT_PULLUP); // ! Comment this line for keyestudio pushbutton
  attachInterrupt(digitalPinToInterrupt(buttonPin), resetMotor, RISING);
  
  // Serial communication between md25 and Arduino/Wemos D1
  Serial.begin(38400);
  
  // Initialize the motor (go the button to reset the encoder value)
  setHome();
}

void loop(){

  minEnc = (int) random(200,3000);
  maxEnc = (int) random(7000,11000); // barre de 4m 11000 / 16000 / 20000 ou 22000

  speedM = getLogNumber();
  
  while(encValue < maxEnc){
    Serial.write(CMD);
    Serial.write(WRITESP2);
    Serial.write(128 + speedM);
    encValue = readEncoder();
  }
 
  speedM = getLogNumber();
  
  while(encValue > minEnc){
    Serial.write(CMD);
    Serial.write(WRITESP2);
    Serial.write(128 - speedM);
    encValue = readEncoder();
  }

}

// Get number from an online text file
int getLogNumber(){
   if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    HTTPClient http;  //Declare an object of class HTTPClient
    http.begin(log_file);  //Specify request destination
    int httpCode = http.GET(); //Send the request
    if (httpCode > 0) { //Check the returning code
      return http.getString().toInt();   //Get the request response payload
    }
    http.end(); //Close connection
  }
}

// Interruption stop motor when the button is switched
void resetMotor() {

  isHome = true;
  
  // Stop
  Serial.write(CMD);
  Serial.write(WRITESP2);
  Serial.write(128); 

  // Reset the encoder registers to 0
  Serial.write(CMD);  
  Serial.write(RESETREG);

  // Moteur bouge dans l'autre sens
  Serial.write(CMD);
  Serial.write(WRITESP2);
  Serial.write(128 + 10);

  delay(1000);
}

void setHome(){
 while( ! isHome){
   Serial.write(CMD);
   Serial.write(WRITESP2);
   Serial.write(128 - 10); // vers le moteur / bouton
 }
}

// Function to read and display the value of both encoders, returns value of first encoder
long readEncoder(){                        
  long result = 0; 
  Serial.write(CMD);
  Serial.write(READENC2);
  while(Serial.available() < 4){} // Wait for 4 bytes, first 4 encoder 1 values second 4 encoder 2 values 
  result = Serial.read();         // First byte for encoder 1, HH.
  result <<= 8;
  result += Serial.read();        // Second byte for encoder 1, HL
  result <<= 8;
  result += Serial.read();        // Third byte for encoder 1, LH
  result <<= 8;
  result += Serial.read();        // Fourth byte for encoder 1, LL
  return result;                                
}
